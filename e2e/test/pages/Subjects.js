
const SUBJECTS_PATH = '/#/subjects-list'
const EDIT_SUBJECT_CLASS = '.solicitude-edit-button'
const SHOW_SUBJECT_CLASS = '.solicitude-show-button'

class Subjects {
  constructor() {
    cy.visit(SUBJECTS_PATH)
    cy.contains('Mis casos')
    cy.reload(true)
    cy.contains('Mis casos')
  }

  editFirstSolicitude() {
    cy.get('.solicitude-edit-button').first().click()
    cy.contains('Solicitante')
    cy.reload(true)
    cy.contains('Solicitante')
    cy.get('#subject').click()

    return this
  }

  editLast() {
    cy.get('.solicitude-edit-button').last().click()
    cy.contains('Solicitante')
    cy.reload(true)
    cy.contains('Solicitante')
    cy.get('#subject').click({force: true})

    return this
  }

  openSubject(){
    cy.get('#edit-subject').click({force: true})
    cy.contains('Guardar')
    cy.reload(true)
    cy.contains('Guardar')
    cy.get('#edit-subject').click({force: true})

    return this
  }

  removeClosedStatus(){
    cy.get('#remove-subject-closed-status').click({force: true})

    return this
  }

  editFirst() {
    cy.get(EDIT_SUBJECT_CLASS).first().click({force: true})
    cy.contains('Solicitante')
    cy.reload(true)
    cy.contains('Solicitante')
    cy.get('#edit-subject').click({force: true})

    return this
  }

  openFirst() {
    cy.get(SHOW_SUBJECT_CLASS).first().click({force: true})

    return this
  }

  fillAnalysis(text) {
    cy.get('#analysis-solicitude').type(text, {force: true})

    return this
  }

  remove() {
    cy.get('#delete-subject').first().click({force: true})

    return this
  }

  removeTopics() {
    cy.get('.delete.icon').first().click({force: true})

    return this
  }

  addSubject(){
    cy.get('#add-subject-button').click({force: true})

    return this
  }

  createSubject(description = "text"){
    this.fillSubject(description)
    cy.get('#create-counseling').click({force: true})

    return this
  }

  fillSubject(description = "text"){
    this.subjectTopic()
      .subjectAnalysis()
      .subjectProposal()
      .subjectDescription(description)

    return this
  }

  subjectTopic() {
    cy.get('#subjects-topics input').type('{enter}', {force:true})
    return this
  }

  addFirstTopic() {
    cy.get('#subjects-topics').click({force: true})
    cy.get('.item.current').first().click({force: true})

    return this
  }

  subjectAnalysis(value = "subject_analysis") {
    cy.get('#analysis-solicitude').type(value, {force: true})
    return this
  }

  subjectProposal() {
    cy.get('#subject-proposals input').type('{enter}', {force:true})
    return this
  }

  subjectDescription(value = "subject_description") {
    cy.get('#solicitude-proposals-description').type(value, {force: true})
    return this
  }

  close() {
    cy.get('#close-modal').click({force: true})

    return this
  }

  chooseReason() {
    cy.get('#close-counseling').click({force: true})

    return this
  }

  confirmChanges() {
    cy.get('#modify-counseling').click({force: true})

    return this
  }

  filter(criteria) {
    cy.get('#select-list input').click({force: true}).type(criteria, {force:true}).type('{enter}', {force:true})
  }

  filterByMyOrganizationLessMine(){
    this.filter('- Míos')

    return this
  }

  filterByMyOrganization(){
    this.filter('Míos +')

    return this
  }

  filterByOtherOrganizations(){
    this.filter('De otras organizaciones')

    return this
  }

  includes(text) {
    cy.contains(text)

    return true
  }

  hasBeenRemoved() {
    cy.contains('Listado de casos')
    cy.get('#subject').should('not.exist')

    return true
  }

  contaisJustified() {
    cy.get('#check').should('exist')

    return true
  }

  notContaisJustified() {
    cy.get('#check').should('not.exist')

    return true
  }

  countsSubjects(number){
    cy.get('#subjects-list>table>tbody')
      .find('tr')
      .should('have.length', number)

    return true
  }
}

export default Subjects
