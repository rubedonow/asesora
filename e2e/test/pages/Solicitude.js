
const COMPANY_NAME_INPUT = '#company-name'
const COMPANY_CIF_INPUT = '#company-cif'
const SOLICITUDE_URL = '/#/solicitude'

class Solicitude {
  constructor(){
    cy.visit(SOLICITUDE_URL)
  }

  hasConfirmationMessage(){
    cy.get('.messageSent')

    return true
  }

  hasPhoneNumberInContact(phonenumber) {
    cy.get('#phonenumber').should('have.value', phonenumber)

    return true
  }

  includes(text){
    cy.contains(text, {force: true})

    return true
  }

  fill(){
    return this
  }

  fillRequired(){
    this.fill()
      .applicantName()
      .applicantPhonenumber()
      .description()

    return this
  }

  fillAll(){
    this.fillRequired()
      .fill()
      .applicantSurname()
      .applicantCCAA()
      .applicantEmail()
      .date()
      .source()
      .companyName()
      .companyCif()
      .companyEmployees()
      .companyCnae()
    return this
  }

  applicantSurname(value = "applicant_surname") {
    cy.get('#surname').type(value, {force: true})
    return this
  }

  applicantName(value = "applicant_name"){
    cy.get('#name').type(value, {force: true})
    return this
  }

  applicantCCAA(value = "Canarias") {
    cy.get('#applicant-ccaa input').click({force:true}).type(value, {force:true}).type('{enter}', {force:true})
    return this
  }

  applicantEmail(value = "a@a.com"){
    cy.get('#email').type(value, {force: true})
    return this
  }

  date(value = "2018-01-01"){
    cy.get('#date').type(value, {force: true})
    return this
  }

  source(value = "Telefónico"){
    cy.get('#source input').click({force:true}).type(value, {force:true}).type('{enter}', {force:true})
    return this
  }

  applicantPhonenumber(value = "666666666"){
    cy.get('#phonenumber').type(value, {force: true})
    return this
  }

  description(value = "sample text"){
    cy.get('#solicitude-text').type(value, {force: true})
    return this
  }

  companyName(value = "company name") {
    cy.get(COMPANY_NAME_INPUT).type(value, {force: true})
    return this
  }

  companyCif(value = '12345678Z') {
    cy.get(COMPANY_CIF_INPUT).type(value, {force: true})
    return this
  }

  companyEmployees(value = '2'){
    cy.get('#company-employees').type(value, {force: true})
    return this
  }

  companyCnae(value = '011'){
    cy.get('#company-cnae input').click({force: true}).type(value, {force:true}).type('{enter}', {force:true})
    return this
  }

  selectFirstContactSuggestion() {
    cy.get('#applicant-matches>div>table>tbody>tr').click({ multiple: true })

    return this
  }

  selectFirstCompanySuggestion() {
    cy.get('#company-matches>div>table>tbody>tr').first().click({ force: true })

    return this
  }

  lostFocus(){
    cy.get('.card-title').click({ multiple: true })
  }

  submit(){
    // this.lostFocus()
    cy.get('#submit').click({force: true})

    return this
  }

  hasCompanyName(text) {
    cy.get(COMPANY_NAME_INPUT).should('have.value', text)

    return true
  }

  isShowingPhoneAlert() {
    cy.get('#contact-phone')

    return true
  }

  isShowingCompanyAlerts() {
    cy.contains('Si se pone CIF se debe poner nombre de empresa para que la solicitud se pueda guardar')
    cy.contains("Debes proporcionar un CIF válido, ejemplo: '12345678Z'")

    return true
  }

  canNotBeCreated() {
    cy.get('#submit').should('be.disabled')

    return true
  }
}
export default Solicitude
