
const USER_MANAGER_PATH = '/#/users-manager'

class UserManager {
  constructor() {
    cy.visit(USER_MANAGER_PATH)
  }

  fillEmail(email) {
    cy.get('#email').type(email)

    return this
  }

  add() {
    cy.get('#email-add-button').click({force: true})

    return this
  }

  removeFirstUser() {
    cy.get('#user-0').click({force: true})

    return this
  }

  includes(text) {
    cy.contains(text)

    return true
  }

  listNotIncludes(email) {
    cy.get('#users-list').should('not.have.value', email)

    return true
  }
}

export default UserManager
