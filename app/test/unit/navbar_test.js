import { shallowMount } from '@vue/test-utils'
import view from '../../src/js/views/navbar'

describe('Navbar', () => {
  let defaultMocks

  beforeEach(function(){
    defaultMocks = {
      labels: { name: '' },
      values: { name: '', imageUrl: '' },
      control: { isAdmin: false }
    }
  })

  it("don't displays 'Gestion de acceso' navlink by default", function() {
    let wrapper = shallowMount(view, { mocks: defaultMocks })

    expect(wrapper.find('#users-manager-link').exists()).to.be.false
  })
  
  it('displays "Gestion de acceso" navlink for admins', function() {
    const mocks = Object.assign({}, defaultMocks, {
      control: { isAdmin: true }
    })

    let wrapper = shallowMount(view, { mocks })

    expect(wrapper.find('#users-manager-link').exists()).to.be.true
  })
  
})
