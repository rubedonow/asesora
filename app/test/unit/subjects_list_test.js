import { shallowMount, mount } from '@vue/test-utils'
import view from '../../src/js/views/subjects-list.vue'
import Models from '../../src/js/models/models'

describe('Subjects List', () => {
  const mockedBus = { publish: () => {} }
  const mocksDefault = Object.assign({}, Models.newSubjectsList(), { bus: mockedBus })
  const DOWNLOAD_ID = "#download-user-counselings"

  it('renders', () => {
    const mocks = mocksDefault
    mount(view, { mocks })
  })

  it("don't shows download link if no subjects", () => {
    const mocks = mocksDefault
    const wrapper = shallowMount(view, { mocks })

    expect(wrapper.find(DOWNLOAD_ID).exists()).to.be.false
  })

  it("shows download link if has subjects", () => {
    const mocks = Object.assign({}, mocksDefault, { hasSubjects: true })
    const wrapper = shallowMount(view, { mocks })

    expect(wrapper.find(DOWNLOAD_ID).exists()).to.be.true
  })

  it("handles download clicks", () => {
    const handler = sinon.spy()
    const mocks = Object.assign({}, mocksDefault, {
      hasSubjects: true
    })
    const methods = {
      handleDownloadClick: handler
    }
    const wrapper = mount(view, { mocks, methods })

    wrapper.find(DOWNLOAD_ID).trigger("click")

    expect(handler).to.be.calledOnce
  })
})
