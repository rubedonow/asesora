import Vue from "vue"
import translations from "./services/translations"

import login from './components/login'
import userSession from './infrastructure/user_session'
import Authorizations from './services/authorizations'

new Authorizations()
translations.retrieve()

new Vue({
  components: { login }
}).$mount('#root')

const { setUser, authorize } = userSession

export const session = { setUser, authorize }