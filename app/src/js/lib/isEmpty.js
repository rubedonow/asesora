
const isEmpty = (value) => {
  const isEmpty = true
  const notEmpty = false

  if (value === null ) { return isEmpty }
  if (value === undefined) { return isEmpty }
  if (value === '') { return isEmpty }
  if (Object.keys(value).length === 0) { return isEmpty }
  if (value.length === 0) { return isEmpty }

  return notEmpty
}

export default isEmpty
