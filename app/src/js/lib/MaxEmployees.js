
export default class MaxEmployees {
  static isBelow(employees) {
    const maxNumberOfEmployees = 32767

    return (employees <= maxNumberOfEmployees)
  }

  static isOver(employees){
    return !this.isBelow(employees)    
  }
}
