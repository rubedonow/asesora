class MapperCatalogs {
  constructor(name, catalog) {
    this.name = name
    this.catalog = catalog
  }

  do() {
    let mapper = {}

    if(this.name == 'topics') {
      mapper = {
        catalogName: this.name,
        catalogsList: this.catalog.map((item) => {
          return {id: item.value.id, name: item.value.name }
        })
      }
    } else {
      mapper = {
        catalogName: this.name,
        catalogsList: this.catalog.map(item => item.text)
      }
    }
    return mapper
  }
}

export default MapperCatalogs
