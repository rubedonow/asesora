import Component from '../infrastructure/component'
import MaxEmployees from '../lib/MaxEmployees'
import MaxFileSize from '../lib/MaxFileSize'
import Models from '../models/models'
import view from '../views/subjects'
import service from '../services/subject'
import Validations from '../services/validations'
import { Bus, Channel } from '../bus'
import { APIClient } from '../infrastructure/api_client'
import { askTranslationsFor } from "../infrastructure/translatable"
import userSession from "../infrastructure/user_session"
import router from "../infrastructure/router"
import Dictionary from './dictionary'
import Catalogs from './catalogs'

export class Subjects extends Component {
  constructor(){

    super()
    new Validations()
    new Catalogs()
    const channel = Channel('subject')
    service(channel).start()
    this.channel = channel
    this.client = APIClient
    this.navigator = router()
    this.defaults = JSON.parse(JSON.stringify(this.data))

    this.channelSubscribe()
  }

  subscribe(){
    Bus.subscribe("got.translation.for.subjects", this.translate.bind(this))
    Bus.subscribe("subjects.view.created", this.initComponent.bind(this))
    Bus.subscribe("subjects.view.destroyed", this.clearSubscriptions.bind(this))
    Bus.subscribe("checked.subject.justified", this.setWarningSubject.bind(this))
    Bus.subscribe('fill.catalogs', this.fillCatalogs.bind(this))
  }

  channelSubscribe() {
    this.channel.subscribe("got.solicitude", this.update.bind(this))
    this.channel.subscribe("subject.created", this.subjectCreated.bind(this))
    this.channel.subscribe("subject.closed", this.subjectClosed.bind(this))
    this.channel.subscribe("deleted.subject", this.deletedSubject.bind(this))
  }

  collectSubscriptions(){
    this.addSubscription('clicked.delete.subject', this.deleteSubject.bind(this))
    this.addSubscription('changed.subject', this.setButtonStatus.bind(this))
    this.addSubscription('changed.subject', this.warningRequired.bind(this))
    this.addSubscription('clicked.close.counseling', this.closeCounseling.bind(this))
    this.addSubscription('subject.create.clicked', this.createSubject.bind(this))
    this.addSubscription('clicked.discard.subject.button', this.discardCard.bind(this))
    this.addSubscription('attached.files', this.attachFiles.bind(this))
  }

  fillCatalogs(payload) {
    for (let [labelKey, valueKey] of Object.entries(payload)) {
      this.data.set(labelKey, valueKey)
    }
  }

  attachFiles(files){
    for (let file of files) {
      if (this.isBelowMaxSize(file) ){
        this.readFile(file)
      }
      else {
        const message = this.data.labels.maxSized.replace('%file', file.name)
        alert(message)
      }
    }
  }

  isBelowMaxSize(file){
    return MaxFileSize.isBelow(file)
  }

  readFile(file) {
    const reader = new FileReader()
    reader.onload = ()=>{this.addFileToModel(reader, file.name)}
    reader.readAsDataURL(file)
  }

  addFileToModel(result, name){
    const rawFile = result.result.split(',')[1]

    const file = { file: rawFile, name: name }
    this.data.values.files.push(file)
  }

  initComponent(solicitude_id){
    this.registerSubscriptions()

    askTranslationsFor({ labels: this.data.labels, scope: "subjects" })
    this.initModel()
    if (solicitude_id) this.edit(solicitude_id)
  }

  edit(solicitude_id){
    this.data.values.solicitudeId = solicitude_id
    this.channel.publish('get.solicitude', solicitude_id)
  }

  initModel(){
    const keys = Object.keys(this.data)
    keys.forEach( key => {
      if (!key.match("labels|values|Catalog") && typeof(this.data[key]) != 'function') {
        this.data[key] = this.defaults[key]
      }
    })

    const valueKeys = Object.keys(this.data.values)
    valueKeys.forEach( key => {
      this.data.values[key] = this.defaults.values[key]
    })
  }

  update({ data }){
    this.data.values.id = data.creation_moment
    let dictionary = this.dictionaryOfSolicitude(data)
    for (let [labelKey, valueKey] of Object.entries(dictionary)) {
      this.data.setValues(labelKey, valueKey)
    }
    const employees = this.data.values.companyEmployees
    this.data.warningEmployees = !employees || !MaxEmployees.isBelow(employees)
    const user = data.user.user_id
    this.data.owner = userSession.isCurrentUser(user)
  }

  discardCard(){
    this.moveCardAnimation('discardCard')
    setTimeout( _ => this.navigator.back(), 1000 )
  }

  moveCardAnimation(cssClass){
    let element = document.querySelector('#subjects')
    element.classList.add(cssClass)
  }

  deletedSubject({ data }){
    let id = data.id
    let subjects = this.data.values.subjects
    this.data.values.subjects = subjects.filter(function(subject){
      return subject.id != id
    })
  }

  setWarningSubject(warning){
    this.data.warningSubject = warning
  }

  createSubject(detail) {
    const subject = Dictionary.subject(detail)
    this.channel.publish('create.subject', subject)
  }

  subjectClosed({ solicitude_id }){
    this.goTo(`/#/show-solicitude/${solicitude_id}`)
  }

  closeCounseling( payload ) {
    const subject = Dictionary.subject(payload)
    this.channel.publish('close.subject', subject)
  }

  deleteSubject(id){
    if (!confirm(this.data.labels.confirmDeleteSubject)){
      return
    }
    this.channel.publish('delete.subject', id)
  }

  setButtonStatus(){
    this.data.isSubmittable = false
    let analysis = (this.data.values.analysis != "")
    let createdAt = (this.data.values.created != "")
    let topics = (this.data.values.selectedTopics.length > 0)
    if(createdAt && (analysis || topics)){
      this.data.isSubmittable = true
    }
  }

  subjectCreated({ solicitude_id }) {
    this.goTo(`#/show-solicitude/${solicitude_id}`)
  }

  gotTopicsCatalog({ data }) {
    let catalog = []
    for (const topic of data) {
      catalog.push({ value: topic, text: topic.name })
    }
    this.data.topicsCatalog = catalog
  }

  gotProposalsCatalog({ data }) {
    let catalog = []
    for (const proposal of data) {
      catalog.push({ value: proposal, text: proposal })
    }
    this.data.proposalsCatalog = catalog
  }

  gotProjectsCatalog( { data } ) {
    let catalog = []
    for (const project of data) {
      catalog.push({ value: project, text: project })
    }
    this.data.projectsCatalog = catalog
  }

  gotReasonsCatalog({ data }) {
    let catalog = []
    for (const reason of data) {
      catalog.push({ value: reason.id, text: reason.name })
    }
    this.data.reasonsCatalog = catalog
    this.data.values.reason = catalog[0]
  }

  dictionaryOfSolicitude(data){
    return {
        'text': data.text,
        'date': data.date,
        'applicantName': data.applicant_name,
        'applicantSurname': data.applicant_surname,
        'applicantCcaa': data.applicant_ccaa,
        'applicantEmail': data.applicant_email,
        'applicantPhonenumber': data.applicant_phonenumber,
        'companyName': data.company_name,
        'companyCif': data.company_cif,
        'companyEmployees': data.company_employees,
        'companyCnae': data.company_cnae,
        'companyId': data.company_id,
        'subjects': data.subjects,
        'source': data.source,
        'organization': data.organization.text,
        'files': []
      }
  }

  warningRequired(){
    this.data.warningSubject = true
    Bus.publish('check.subject.justified', this.data.values)
  }

  model(){
    if (!this.data) { this.data = Models.newSubjects() }

    return this.data
  }

  view(){
    return view
  }
}

const component = new Subjects()

export default Object.assign({}, component.view, { data: component.model })
