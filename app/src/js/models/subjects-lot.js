import Model from './model'
import {Bus} from '../bus'

export default class SubjectsLot extends Model {
  constructor() {
    super()
    this.labels = {
      title: "",
      project: "",
      subjectNumber: "",
      subjectDate: "",
      ownerName: "",
      companyName: "",
      topics: "",
      register: "",
      isJustified: "",
      show: "",
      notApply: "",
      registerInfoA: "",
      registerInfoD: "",
      registerInfoP: "",
      subjectWarning: "",
      subjectCheck: ""
    }
    this.subjectsList = []
    this.hasSubjects = false
    this.bus = Bus
  }
}
