import Model from './model'

export default class Login extends Model {
  constructor() {
    super()
    this.labels = {
      "title": "",
      "instructions": "",
      "unauthorizedEmail": ""
    },
    this.values = {
      "error": ""
    }
  }
}
