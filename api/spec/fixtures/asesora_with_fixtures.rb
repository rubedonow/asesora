require_relative '../../system/services/translation/collection'
require_relative '../../asesora'
require 'json'

require_relative './fixtures'

class AsesoraWithFixtures < Asesora
  def self.locale
    Translation::Collection::DEFAULT_LOCALE
  end

  post '/fixtures/pristine' do
    fixtures = Fixtures.new.pristine

    { 'fixtures' => fixtures }.to_json
  end

  post '/fixtures/clean' do
    Fixtures.new.clean_collections

    { 'fixtures': [] }.to_json
  end

  post '/fixtures/clean-sftp' do
    Fixtures.new.drop_sftp

    { 'fixtures': [] }.to_json
  end

  post '/fixtures/reset_authorized_users_catalog' do
    Fixtures.new.reset_authorized_users_catalog

    { 'fixtures': [] }.to_json
  end
end
