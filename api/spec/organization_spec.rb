require_relative '../system/infrastructure/clients'
require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'
require 'rack/test'
require 'json'
require 'rspec'

describe 'Organization API' do
  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  before(:each) do
    post 'fixtures/clean'
  end

  after do
    post 'fixtures/clean'
  end

  it 'is created with a solicitude' do
    domain = 'organization.com'
    solicitude = { "date" => '', "user_id": 'user@' + domain }

    post '/api/create-solicitude', solicitude.to_json

    organization = organizations.first
    expect(organization['domain']).to eq(domain)
  end

  it 'creates an organization by domain' do
    solicitude = { "date" => '', "user_id": 'user@organization.com' }

    post '/api/create-solicitude', solicitude.to_json
    post '/api/create-solicitude', solicitude.to_json

    expect(organizations.count).to eq(1)
  end

  it 'creates organizations' do
    solicitude = { "date" => '', "user_id": 'user@organization.com' }
    another_solicitude = { "date" => '', "user_id": 'user@anotherorganization.com' }

    post '/api/create-solicitude', solicitude.to_json
    post '/api/create-solicitude', another_solicitude.to_json

    expect(organizations.count).to eq(2)
  end

  def organizations
    client[:organizations].find()
  end

  def client
    @client ||= Infrastructure::Clients.mongo
  end
end
