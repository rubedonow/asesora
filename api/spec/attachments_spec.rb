require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'
require 'rack/test'
require 'rspec'

describe 'Attachments' do
  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  before(:each) do
    Fixtures.new.drop_attachments
    Fixtures.new.drop_sftp
  end

  after(:all) do
    Fixtures.new.drop_attachments
    Fixtures.new.drop_sftp
  end

  it "downloads an attachment" do
    attachment = Attachments::Service.create(Fixtures::FILE, 'subject_id')

    get '/api/download/' + attachment['id']

    file_content = last_response.body
    expect(file_content).to eq(Fixtures::FILE_CONTENT)
  end

  it "deletes an attachment" do
    subject_id = 'subject_id'
    attachment = Attachments::Service.create(Fixtures::FILE, subject_id)

    post '/api/delete-attachment', {id: attachment['id']}.to_json

    attachments = Attachments::Service.retrieve(subject_id)
    expect(attachments).to be_empty
  end
end
