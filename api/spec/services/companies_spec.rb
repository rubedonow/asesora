require_relative '../../system/services/companies/service'
require_relative '../../system/infrastructure/clients'
require_relative '../fixtures/fixtures'

describe 'Companies' do
  before(:each) do
    Fixtures.new.drop_companies
  end

  after do
    Fixtures.new.drop_companies
  end

  it 'can create different companies without CIF' do

    Companies::Service.create('a company', nil, nil, '', '')
    Companies::Service.create('another company', nil, nil, '', '')

    expect(companies.count).to eq(2)
  end

  def companies
    collection.find()
  end

  def collection
    @client ||= Infrastructure::Clients.mongo

    @client[:companies]
  end
end
