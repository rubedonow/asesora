require_relative  '../../system/services/catalogs/service'
require 'json'

describe 'Catalogs services' do
  it 'update catalog type hash' do
    new_value = "Carta por correo"
    old_value = "Presencial en sede"
    catalog = "source_formats"

    source_catalog = Catalogs::Service.update_catalog(new_value, old_value, catalog, dry_run = true)
    expected_catalog = source_catalog[:content].map{|element|element["name"]}

    expect(expected_catalog).to include(new_value)
  end

  it 'adds item to catalog type hash' do
    new_value = "Carta por correo2"
    catalog = "source_formats"
    source_old_catalog = Catalogs::Service.source_formats()

    source_updated_catalog = Catalogs::Service.add_item_to_catalog(new_value, catalog, dry_run = true)

    found_element = contains_value?(source_updated_catalog[:content], new_value)
    expect(found_element).to be true
    increased = source_updated_catalog[:content].size - source_old_catalog[:content].size
    expect(increased).to eq 1
  end

  xit 'does not add the same item twice' do
    new_value = "Crorizoas3"
    catalog = "source_formats"
    source_old_catalog = Catalogs::Service.source_formats()

    Catalogs::Service.add_item_to_catalog(new_value, catalog, dry_run = false)
    source_updated_catalog = Catalogs::Service.add_item_to_catalog(new_value, catalog, dry_run = false)

    increased = source_updated_catalog[:content].size - source_old_catalog[:content].size
    expect(increased).to eq 1
  end

  it 'update catalog type array' do
    new_value = "Proyecto 5"
    old_value = "Proyecto 4"
    catalog = "projects"

    projects_catalog = Catalogs::Service.update_catalog(new_value, old_value, catalog, dry_run = true)
    expected_catalog = projects_catalog[:content]

    expect(expected_catalog).to include(new_value)
  end

  it 'adds item to catalog type array' do
    new_value = "Proyecto 77"
    catalog = "projects"
    projects_old_catalog = Catalogs::Service.projects()

    projects_updated_catalog = Catalogs::Service.add_item_to_catalog(new_value, catalog, dry_run = true)

    found_element =  projects_updated_catalog[:content].include?(new_value)

    expect(found_element).to be true
    increased = projects_updated_catalog[:content].size - projects_old_catalog[:content].size
    expect(increased).to eq 1
  end

  it 'update catalog type dictionary' do
    new_value = "Comissions"
    old_value = "Comissions Obreres de les Illes Balears - TERRITORIOS"
    catalog = "organizations"

    organizations_catalog = Catalogs::Service.update_catalog(new_value, old_value, catalog, dry_run = true)
    expected_catalog_name = organizations_catalog[:content].map{|element|element["name"]}

    expect(expected_catalog_name).to include(new_value)
  end

  it 'update catalog cnae dictionary' do
    new_value = "Agricultura"
    old_value = "Agricultura, ganadería, caza y servicios relacionados con las mismas"
    catalog = "cnae"

    cnae_catalog = Catalogs::Service.update_catalog(new_value, old_value, catalog, dry_run = true)
    expected_catalog_id = cnae_catalog[:content].map{|element|element["id"]}
    expected_catalog_name = cnae_catalog[:content].map{|element|element["name"]}

    expect(expected_catalog_name).to include(new_value)
  end

  it 'return the ccaa to justify' do
    catalog_name = "ccaa"
    id_catalog = "01"
    value_feprl = "ANDALUCIA"

    expected_feprl = Catalogs::Service.get_FEPRL_catalog(catalog_name, id_catalog)

    expect(expected_feprl).to eq(value_feprl)
  end

  it 'return the cnae to justify' do
    catalog_name = "cnae"
    id_catalog = "010"
    value_feprl = "01 Agricultura; ganadería; caza y servicios relacionados con las mismas"

    expected_feprl = Catalogs::Service.get_FEPRL_catalog(catalog_name, id_catalog)

    expect(expected_feprl).to eq(value_feprl)
  end

  it 'returns true if user is coordinator' do
    user_id = "coordinator@gmail.com"

    response = Catalogs::Service.user_coordinator?(user_id)

    expect(response).to be true
  end

  it 'returns true if user is not coordinator' do
    user_id = "not@gmail.com"

    response = Catalogs::Service.user_coordinator?(user_id)

    expect(response).to be false
  end

  private

  def contains_value?(catalog, value)
    catalog.any? {|item| item["name"] == value}
  end
end
