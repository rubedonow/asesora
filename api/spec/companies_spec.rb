require 'rspec'
require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'
require_relative '../system/services/companies/service'

describe 'Companies' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  before(:each) do
	  post 'fixtures/clean'
  end

  after(:each) do
    post 'fixtures/clean'
  end

	it 'returns a list filtered by name and cnae' do
    first_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "First company",
			"companyCif": Fixtures::COMPANY_CIF_2,
			"companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(first_solicitude)

		second_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "Second company",
			"companyCif": Fixtures::COMPANY_CIF_3,
			"companyCnae": "",
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(second_solicitude)

    third_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "Third comp",
			"companyCif": Fixtures::COMPANY_CIF,
			"companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(third_solicitude)

		body = {
			"companyName": "Pán",
			"companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_company_matches(body)
		filtered_companies_list = JSON.parse(last_response.body)

    expect(filtered_companies_list["data"].count).to eq(1)
		expect(filtered_companies_list["data"][0]['name']).to eq("First company")
	end

	it 'searches by name when cnae is empty' do
		first_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "First company",
			"companyCif": Fixtures::COMPANY_CIF_2,
			"companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(first_solicitude)

		second_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "Last",
			"companyCif": Fixtures::COMPANY_CIF_3,
			"companyCnae": Fixtures::COMPANY_CNAE_2,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(second_solicitude)

		body = {
			"companyName": "com",
			"companyCnae": "",
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_company_matches(body)
		filtered_companies_list = JSON.parse(last_response.body)

		expect(filtered_companies_list["data"].count).to be == 1
	end

  it 'search by outdated company name return zero matches' do
    name = Fixtures::COMPANY_NAME
    cif = Fixtures::COMPANY_CIF
    employees = Fixtures::COMPANY_EMPLOYEES
    cnae = Fixtures::COMPANY_CNAE
    cp = Fixtures::COMPANY_CP
    company = Companies::Service.create(name, cif, employees, cnae, cp)

    updated_name = Fixtures::COMPANY_NAME_2
    updated_employees = Fixtures::COMPANY_EMPLOYEES_2
    Companies::Service.update(updated_name, cif, updated_employees, cnae, cp, company['id'])

    criteria = {
      name: Fixtures::COMPANY_NAME,
      cnae: "",
      "user_id": "asesora@ccoo.es"
    }
    company_list = Companies::Service.all(criteria)

		expect(company_list.size).to eq(0)
  end


  it 'filter by domain when search company matches' do
    first_solicitude = {
			"applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
			"text": Fixtures::TEXT,
			"date": Fixtures::DATE,
			"applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
			"companyName": "Company One",
			"companyCif": Fixtures::COMPANY_CIF_2,
			"companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ccoo.es"
		}.to_json

		post_create_solicitude(first_solicitude)

    second_solicitude = {
      "applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
      "text": Fixtures::TEXT,
      "date": Fixtures::DATE,
      "applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
      "companyName": "Company Two",
      "companyCif": Fixtures::COMPANY_CIF,
      "companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "persona@ccoo.es"
    }.to_json

    post_create_solicitude(second_solicitude)

    second_solicitude = {
      "applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
      "text": Fixtures::TEXT,
      "date": Fixtures::DATE,
      "applicantEmail": Fixtures::APPLICANT_EMAIL,
      'applicantId': "",
      "companyName": "Company Three",
      "companyCif": Fixtures::COMPANY_CIF,
      "companyCnae": Fixtures::COMPANY_CNAE,
      "user_id": "asesora@ugt.es"
    }.to_json

    post_create_solicitude(second_solicitude)

		body = {
			"companyName": "com",
			"companyCnae": "",
      "user_id": "person@ccoo.es"
		}.to_json

		post_company_matches(body)

		filtered_companies_list = JSON.parse(last_response.body)

    expect(filtered_companies_list["data"].count).to be == 2
		expect(filtered_companies_list["data"][0]['name']).to eq("Company One")
  end

	private

	def post_create_solicitude(body_created)
    	post '/api/create-solicitude', body_created
	end

	def post_company_matches(filtered)
		post "/api/company-matches", filtered
	end
end
