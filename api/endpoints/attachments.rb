require_relative '../system/actions/download_attachment'
require_relative '../system/actions/delete_attachment'
require 'json'

module Endpoints
  class Attachments
    def self.define_endpoints(api)
      api.get '/api/download/:id' do |id|
        downloaded = Actions::DownloadAttachment.do(id)

        send_file downloaded['path'], :filename => downloaded['name']
      end

      api.post '/api/delete-attachment' do
        payload = JSON.parse(request.body.read, {:symbolize_names => true})
        message = Actions::DeleteAttachment.do(payload)
        message.to_json
      end
    end
  end
end
