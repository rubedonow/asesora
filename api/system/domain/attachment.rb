require 'securerandom'

module Domain
  class NullAttachment
    def serialize
      return {}
    end

    def nil?
      true
    end
  end

  class Attachment
    TEMPORALE_FILE_PATH = './temporary_files/file.temp'

    def initialize(name, id=nil, subject_id)
      @name = name
      @id = id || generate
      @subject_id = subject_id
    end

    def self.from_document(document)
      attachment = new(document['name'], document['id'], document['subject_id'])
      attachment
    end

    def self.with(name, id=nil, subject_id)
      attachment = new(name, id, subject_id)
      attachment
    end

    def self.nullified
      return NullAttachment.new
    end

    def sftp_filename
      @id + '_' + @name
    end

    def path
      TEMPORALE_FILE_PATH
    end

    def filename
      @name
    end

    def identify
      @id
    end

    def serialize
      {
        "id" => @id,
        "name" => @name,
        "subject_id" => @subject_id,
        "path" => TEMPORALE_FILE_PATH
      }
    end

    private

    def generate
      SecureRandom.uuid
    end
  end
end
