require_relative '../services/organizations/service'
require_relative '../services/attachments/service'
require_relative '../services/solicitudes/service'
require_relative '../services/subjects/service'

module Actions
  class RetrieveSubjectsByDomain
    class << self
      def do(user)
        solicitudes = solicitudes_for(user)
        subjects = extract_subjects_from(solicitudes)

        subjects
      end

      def less_do(user)
        solicitudes = ::Solicitudes::Service.all_by_domain_less(user)
        subjects = extract_subjects_from(solicitudes)

        subjects
      end

      def less_mine_do(user)
        solicitudes = ::Solicitudes::Service.all_less_my_domain_less(user)
        subjects = extract_subjects_from(solicitudes)

        subjects
      end

      def all_do(user)
        domain = ::Organizations::Service.domain_in(user)

        solicitudes_by_domain = ::Solicitudes::Service.all_by_domain(domain)
        all_solicitudes = ::Solicitudes::Service.all_less_my_domain_less(user)
        my_subjects = extract_subjects_from(solicitudes_by_domain)
        all_subjects = extract_subjects_from(all_solicitudes)

        subjects = my_subjects.push(all_subjects)

        subjects.flatten
      end

      private

      def remove_my_solicitudes(solicitudes, user)
        solicitudes.select do |solicitude|
          solicitude['user']['user_id'] != user
        end
      end

      def solicitudes_for(user)
        domain = ::Organizations::Service.domain_in(user)

        solicitudes = ::Solicitudes::Service.all_by_domain(domain)

        solicitudes
      end

      def extract_subjects_from(solicitudes)

        result = solicitudes.map do |solicitude|
          subjects = ::Subjects::Service.all_by(solicitude['creation_moment'])

          subjects.map do |subject|
            subject['solicitude_numeration'] = solicitude['numeration']
            subject['source'] = solicitude["source"]
            subject['user_id'] = solicitude['user']['user_id']
            subject['solicitude_id'] = solicitude["creation_moment"]
            subject['date'] = solicitude["date"]

            company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
            subject['company_name'] = company["name"]
            subject['employees'] = company["employees"]
            subject['cnae'] = company["cnae"]

            applicant = ::Applicant::Service.retrieve(solicitude['applicant'])
            subject['ccaa'] = applicant["ccaa"]

            attachments = ::Attachments::Service.retrieve(subject['id'])
            subject["files"] = attachments

            subject['subject_id'] = subject["id"]

            subject
          end
        end

        result.flatten
      end
    end
  end
end
