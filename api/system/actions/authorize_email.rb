require_relative '../services/authorization/service'

module Actions
  class AuthorizeEmail
    def self.do(payload)
      error = ''
      token = ::Authorization::Service.token_for(payload)

      if token == ""
        error = 'Not authorized email'
      end

      {error: error, token: token}
    end

    def self.do_email(payload)
      email = ::Authorization::Service.valid_email(payload)
      email
    end
  end
end
