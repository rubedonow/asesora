require_relative '../services/solicitudes/service'

module Actions
  class UpdateSolicitude
    def self.do(date:, text:, source:, id:, company_cif:, user_id:, domain:, creation_moment:, company_id:, numeration:, organization:, shared_origin:)
      applicant_id = id
      ::Solicitudes::Service.update(date, text, source, applicant_id, company_id, user_id, domain, numeration, organization, shared_origin, creation_moment)
    end
  end
end
