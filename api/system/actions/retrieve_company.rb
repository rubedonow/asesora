require_relative '../services/companies/service'

module Actions
  class RetrieveCompany
    def self.do(cif:)
      companies = ::Companies::Service.all_by({'cif' => cif})
      return [] if companies.empty?

      companies.first
    end
  end
end
