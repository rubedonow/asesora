require_relative '../services/companies/service'

module Actions
  class UpdateCompany
    def self.do(company_name:, company_cif:, company_employees:, company_cnae:, company_cp:, company_id:)
      ::Companies::Service.update(company_name, company_cif, company_employees, company_cnae, company_cp, company_id)
    end
  end
end
