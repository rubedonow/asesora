require_relative './mergeable'

module Actions
  class SolicitudePresenter

    include Mergeable

    def initialize(solicitude, company, applicant, subjects, meetsFeprl=nil)
      @solicitude = solicitude
      @company = company
      @applicant = applicant
      @subjects = subjects
      @meetsFeprl = meetsFeprl
    end

    def serialize(format: :full)
      return short if format == :short

      full
    end

    private

    def full
      prepared_solicitude = add_with_prefix(@solicitude, @company,'company')
      prepared_applicant = add_with_prefix(@solicitude, @applicant,'applicant')
      @solicitude = add(prepared_solicitude, prepared_applicant)
      @solicitude[:subjects] = @subjects
      @solicitude = add(@solicitude, {meetsFeprl: @meetsFeprl})
      @solicitude
    end

    def short
      prepared_solicitude = add_with_prefix(@solicitude, @company,'company')
      prepared_solicitude = add(prepared_solicitude, @applicant)
      if @subjects.count > 0
        prepared_solicitude[:subjects] = @subjects
      end

      prepared_solicitude
    end
  end
end
