require_relative '../services/organizations/service'
require_relative '../services/solicitudes/service'
require_relative '../services/companies/service'
require_relative '../services/applicant/service'
require_relative '../services/subjects/service'
require_relative '../services/attachments/service'
require_relative '../libraries/email'

module Actions
  class CreateSolicitudeFromSubject
    class << self

      def do(text:, email:, user_id:, shared_origin:)
        applicant = get_applicant(email)
        source = {"value": "05","text": "Telemática"}

        subject = share_subject(text, source, applicant, user_id, shared_origin)

        subject
      end

      private

      def share_subject(text, source, applicant, user_id, shared_origin)
        shared_solicitude = create_shared_solicitude(text, source, applicant, user_id, shared_origin)

        updated_subject = add_destination_to_subject(shared_origin, shared_solicitude['creation_moment'], shared_solicitude['numeration'])
        updated_subject['files'] = attachments_for(updated_subject['id'])

        updated_subject
      end

      def add_destination_to_subject(subject_id, destination_solicitude_id, destination_solicitude_numeration )
        subject = ::Subjects::Service.add_destination(subject_id, destination_solicitude_id, destination_solicitude_numeration)

        subject
      end

      def create_shared_solicitude(text, source, applicant, user_id, shared_origin)
        shared_origin = retrieve_origin_data(shared_origin)

        domain = Email.extract_domain(user_id)
        create_organization_for(user_id)
        numeration = next_numeration(user_id)
        company = create_empty_company()
        date = ''
        organization = ''

        solicitude = ::Solicitudes::Service.create(
          date,
          text,
          source,
          applicant['id'],
          company['id'],
          user_id,
          domain,
          numeration,
          organization,
          shared_origin
        )

        solicitude
      end

      def retrieve_origin_data(id)
        subject = ::Subjects::Service.retrieve(id)
        origin_data = {id: subject['id'],
                            solicitude_id: subject['solicitude_id'],
                            numeration: subject['numeration']}

        origin_data
      end

      def attachments_for(subject_id)
        ::Attachments::Service.retrieve(subject_id)
      end

      def create_empty_company()
        ::Companies::Service.create('', '', '', '', '')
      end

      def get_applicant(email)
        criteria = { email: email }
        applicant = Applicant::Service.all_by(criteria)

        if applicant.any?
          applicant = applicant.first
        else
          applicant = create_applicant('', '', email, '', '')
        end

        applicant
      end

      def create_organization_for(email)
        ::Organizations::Service.create(email)
      end

      def create_applicant(name, surname, email, phonenumber, ccaa)
        ::Applicant::Service.create(
          name,
          surname,
          email,
          phonenumber,
          ccaa
        )
      end

      def next_numeration(email)
        numeration = ::Organizations::Service.next_solicitude_numeration(email)

        numeration
      end
    end
  end
end
