require_relative '../../domain/solicitude'
require_relative '../../libraries/email'
require_relative '../../domain/user'
require_relative 'collection'

module Solicitudes
  class Service
    class << self
      def create(date, text, source, applicant, company, user_id, domain, numeration, organization, shared_origin)
        user = Domain::User.with(user_id, domain)
        solicitude = Domain::Solicitude.with(date, text, source, applicant, company, user, numeration, organization, shared_origin)

        Collection.create(solicitude).serialize
      end

      def retrieve(id)
        solicitude = Collection.retrieve(id)
        return {} if solicitude.nil?
        solicitude.serialize
      end

      def delete(id)
        Collection.delete(id)
      end

      def update(date, text, source, applicant, company, user_id, domain, numeration, organization, shared_origin, creation_moment)
        solicitude = Collection.retrieve(creation_moment)
        return solicitude.serialize unless solicitude.belongs?(user_id)
        return Domain::Solicitude.as_null if applicant.nil?
        user = Domain::User.with(user_id, domain)
        solicitude = Domain::Solicitude.with(date, text, source, applicant, company, user, numeration, organization, shared_origin, creation_moment, nil)
        Collection.update(creation_moment, solicitude).serialize
      end

      def times_company(id)
        return 0 if id.nil?

        Collection.times_company(id)
      end

      def all
        Collection.all.serialize
      end

      def all_by(criteria)
        solicitudes = Collection.all_by(criteria)

        serialize(solicitudes)
      end

      def all_by_user(user_id)
        Collection.all_by_user(user_id).serialize
      end

      def all_by_domain(domain)
        Collection.all_by_domain(domain).serialize
      end

      def all_by_domain_less(user)
        domain = Email.extract_domain(user)
        solicitudes = Collection.all_by_domain(domain)

        selected = solicitudes.select do |solicitude|
          !solicitude.belongs?(user)
        end

        serialize(selected)
      end

      def all_less_my_domain_less(user)
        domain = Email.extract_domain(user)
        solicitudes = Collection.all

        selected = solicitudes.select do |solicitude|
          !solicitude.belongs_organization?(domain)
        end

        serialize(selected)
      end

      def checksFeprl(date, company, applicant)
        result = false
        return result if !company or !applicant
        result = true if date != '' and company['cnae'] and company['employees'] and applicant['ccaa']

        result
      end

      private

      def serialize(solicitudes)
        solicitudes.map do |solicitude|
          solicitude.serialize
        end
      end
    end
  end
end
